# A Surfeit of SSH Cipher Suites (Data) #

This repository contains scan data for two internet-wide scans used in

- Albrecht, M. R., Degabriele, J. P., Hansen, T. B., & Paterson, K. G. (2016). A surfeit of SSH
  cipher suites. In E. R. Weippl, S. Katzenbeisser, C. Kruegel, A. C. Myers, & S. Halevi, ACM CCS 16
  (pp. 1480–1491). : ACM Press.

All json files were produced by calling the also included `parse.py` script on a ZGrap output file (also in json). See `example-2015-12.json` for an example of such a file (where we replaced all IP addresses by `127.0.0.1`).
