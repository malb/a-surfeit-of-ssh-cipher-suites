#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from collections import OrderedDict
import json
import sys
import re

json_args = {"indent": 4,
             "sort_keys": False}


def parse_comments(filename, filterstr=None):
    oses = {"OpenSSH": {},
            "dropbear": {},
            "ROSSSH": {},
            "ARRIS": {},
            "Other": {}}

    fh = open(filename)

    seen_software = set()

    while True:
        line = fh.readline()
        if not line:
            break
        host = json.loads(line)
        try:
            software_version = host["data"]["ssh"]["server_protocol"]["software_version"]
            comments = host["data"]["ssh"]["server_protocol"].get("comments", "")
        except KeyError:
            continue

        if filterstr and not software_version.startswith(filterstr):
            continue

        if software_version.startswith("OpenSSH"):
            software = "OpenSSH"
        elif software_version.startswith("dropbear"):
            software = "dropbear"
        elif software_version.startswith("ROSSSH"):
            software = "ROSSSH"
        elif software_version.startswith("ARRIS"):
            software = "ARRIS"
        else:
            if software_version not in seen_software:
                sys.stderr.write("ignoring software '%s'\n"%software_version)
                seen_software.add(software_version)
            software = "Other"

        oses[software][comments] = oses[software].get(comments, 0) + 1

    print json.dumps(oses, **json_args)


def parse_vulnerable(filename, new_attack=False):
    """Parse number of servers vulnerable to plaintext recovery attack

    :param filename: json filename
    :param new_attack: also consider new timing attack
    """

    fh = open(filename)
    seen_software = set()

    vulnerable_servers = {"OpenSSH": 0, "dropbear": 0}
    all_servers = {"OpenSSH": 0, "dropbear": 0}

    while True:
        line = fh.readline()
        if not line:
            break
        host = json.loads(line)
        try:
            software_version = host["data"]["ssh"]["server_protocol"]["software_version"]
            kex_init = host["data"]["ssh"]["server_key_exchange_init"]
        except KeyError:
            continue

        if software_version.startswith("OpenSSH_"):
            software = "OpenSSH"
        elif software_version.startswith("dropbear"):
            software = "dropbear"
        else:
            if software_version not in seen_software:
                sys.stderr.write("ignoring software '%s' "%(software_version,))

                if all_servers["OpenSSH"] == 0:
                    continue
                sys.stderr.write("OpenSSH %d (%4.1f%%), "%(vulnerable_servers["OpenSSH"],
                                                           100*float(vulnerable_servers["OpenSSH"])/all_servers["OpenSSH"]))
                sys.stderr.write("dropbear %d (%4.1f%%)\n"%(vulnerable_servers["dropbear"],
                                                            100*float(vulnerable_servers["dropbear"])/all_servers["dropbear"]))
                seen_software.add(software_version)
            continue

        algorithm = kex_init["encryption_client_to_server"][0]
        all_servers[software] +=1

        if algorithm.endswith("cbc"):
            version = software_version.split("_")[-1]
            if new_attack is True:
                vulnerable_servers[software] += 1
            elif software == "dropbear":
                vulnerable_servers[software] += 1
            elif software == "OpenSSH" and re.match("[0-4].*", version):
                vulnerable_servers[software] += 1
            elif software == "OpenSSH" and re.match("5\.[0-1].*", version):
                vulnerable_servers[software] += 1

    servers = {}
    for key in vulnerable_servers:
        servers[key] = (vulnerable_servers[key], all_servers[key])

    print json.dumps(servers, **json_args)


def parse_modes(filename):
    modes = {"OpenSSH": {"CBC": 0, "CTR": 0, "Other": 0},
             "dropbear": {"CBC": 0, "CTR": 0, "Other": 0},
             "ROSSSH": {"CBC": 0, "CTR": 0, "Other": 0},
             "ARRIS": {"CBC": 0, "CTR": 0, "Other": 0},
             "Other": {"CBC": 0, "CTR": 0, "Other": 0}}

    fh = open(filename)

    seen_software = set()
    seen_algorithms = set()

    while True:
        line = fh.readline()
        if not line:
            break
        host = json.loads(line)
        try:
            software_version = host["data"]["ssh"]["server_protocol"]["software_version"]
            kex_init = host["data"]["ssh"]["server_key_exchange_init"]
        except KeyError:
            continue

        if software_version.startswith("OpenSSH"):
            software = "OpenSSH"
        elif software_version.startswith("dropbear"):
            software = "dropbear"
        elif software_version.startswith("ROSSSH"):
            software = "ROSSSH"
        elif software_version.startswith("ARRIS"):
            software = "ARRIS"
        else:
            if software_version not in seen_software:
                sys.stderr.write("ignoring software '%s'\n"%software_version)
                seen_software.add(software_version)
            software = "Other"

        algorithm = kex_init["encryption_client_to_server"][0]

        if algorithm.endswith("ctr"):
            algorithm = "CTR"
        elif algorithm.endswith("cbc"):
            algorithm = "CBC"
        else:
            if algorithm not in seen_algorithms:
                sys.stderr.write("ignoring algorithm '%s'\n"%algorithm)
                seen_algorithms.add(algorithm)
            algorithm = "Other"

        modes[software][algorithm] += 1

    print json.dumps(modes, **json_args)


def parse_modes_details(filename, direction="server"):
    openssh = {}
    dropbear = {}
    overall = {}

    fh = open(filename)

    while True:
        line = fh.readline()
        if not line:
            break
        host = json.loads(line)
        try:
            software_version = host["data"]["ssh"]["server_protocol"]["software_version"]
            kex_init = host["data"]["ssh"]["server_key_exchange_init"]
        except KeyError:
            continue

        if software_version.startswith("OpenSSH"):
            software = "OpenSSH"
        elif software_version.startswith("dropbear"):
            software = "dropbear"
        else:
            software = "Other"

        if direction == "server":
            enc = kex_init["encryption_client_to_server"][0]
            mac = kex_init["mac_client_to_server"][0]
        elif direction == "client":
            enc = kex_init["encryption_server_to_client"][0]
            mac = kex_init["mac_server_to_client"][0]
        else:
            raise ValueError("Direction '%s' not understood"%direction)

        encmac = "%s + %s"%(enc, mac)

        if software == "OpenSSH":
            openssh[encmac] = openssh.get(encmac, 0) + 1
        elif software == "dropbear":
            dropbear[encmac] = dropbear.get(encmac, 0) + 1

        overall[encmac] = overall.get(encmac, 0) + 1

    overall = OrderedDict(sorted(overall.iteritems(), key=lambda (x, y): y, reverse=True))
    openssh = OrderedDict(sorted(openssh.iteritems(), key=lambda (x, y): y, reverse=True))
    dropbear = OrderedDict(sorted(dropbear.iteritems(), key=lambda (x, y): y, reverse=True))

    print json.dumps(OrderedDict([("overall", overall),
                                  ("openssh", openssh),
                                  ("dropbear", dropbear)]), **json_args)


def parse_servers(filename, filter_mode=None):

    servers = {}
    fh = open(filename)

    while True:
        line = fh.readline()
        if not line:
            break
        host = json.loads(line)
        try:
            software_version = host["data"]["ssh"]["server_protocol"]["software_version"]
            kex_init = host["data"]["ssh"]["server_key_exchange_init"]

            if filter_mode:
                mode = kex_init["encryption_client_to_server"][0]
                if mode.endswith("ctr"):
                    mode = "CTR"
                elif mode.endswith("cbc"):
                    mode = "CBC"
                else:
                    mode = "Other"
                if filter_mode != mode:
                    continue
            servers[software_version] = servers.get(software_version, 0) + 1
        except KeyError:
            pass
    servers = sorted(servers.iteritems(), key=lambda x: x[1], reverse=True)
    servers = OrderedDict(servers)
    print json.dumps(servers, **json_args)


def parse_ciphers(filename):
    encryption_c2s = {}
    encryption_s2c = {}

    fh = open(filename)

    while True:
        line = fh.readline()
        if not line:
            break
        host = json.loads(line)
        try:
            software_version = host["data"]["ssh"]["server_protocol"]["software_version"]
            kex_init = host["data"]["ssh"]["server_key_exchange_init"]
        except KeyError:
            continue

        algorithms = [kex_init["encryption_client_to_server"],
                      kex_init["encryption_server_to_client"]]

        for i, enc in enumerate([encryption_c2s, encryption_s2c]):
            if software_version not in enc:
                enc[software_version] = {}
            alg = algorithms[i][0]
            enc[software_version][alg] = enc[software_version].get(alg, 0) + 1

    for i, enc in enumerate([encryption_c2s, encryption_s2c]):
        enc = sorted(enc.iteritems(), key=lambda x: x[0])
        enc = OrderedDict(enc)
        for software in enc:
            enc[software] = OrderedDict(sorted(enc[software].iteritems(), key=lambda x: x[1]))
        print json.dumps(enc, **json_args)


def extract_random_ips_for_software_version(filename, software_version, count):
    ips = []
    fh = open(filename)

    while True:
        line = fh.readline()
        if not line:
            break
        host = json.loads(line)
        try:
            software_version = host["data"]["ssh"]["server_protocol"]["software_version"]
        except KeyError:
            continue
        if software_version == software_version:
            ip = host["ip"]
            ips.append(ip)

    import random
    random.shuffle(ips)
    for i in range(count):
        print ips[i]


def parse_arguments():
    parser = argparse.ArgumentParser(description='Parse SSH scan results')
    parser.add_argument('-c', '--count', help='number of IP addresses to extract',
                        dest="ip_count")
    parser.add_argument('-s', '--software_version', help='limit to this software version',
                        dest="software_version", default="")
    parser.add_argument('-m', '--mode', help='limit to this mode (e.g. CBC)',
                        dest="modes", default="")
    parser.add_argument('-n', '--new_attack', help='consider also new timing attack',
                        dest="new_attack", action="store_true")
    parser.add_argument('-d', '--direction', help="ciphers to 'server' or to 'client'",
                        dest="direction", default="server")

    parser.add_argument('command', help="one of 'servers', 'ciphers', 'modes', 'ips', 'comments', 'modes_details'")
    parser.add_argument('input', help="zscan json output filename")

    return parser.parse_args()

if __name__ == "__main__":
    args = parse_arguments()

    if args.command == "servers":
        parse_servers(args.input, args.modes)
    elif args.command == "ciphers":
        parse_ciphers(args.input)
    elif args.command == "modes":
        parse_modes(args.input)
    elif args.command == "modes_details":
        parse_modes_details(args.input, direction=args.direction)
    elif args.command == "comments":
        parse_comments(args.input)
    elif args.command == "vulnerable":
        parse_vulnerable(args.input, args.new_attack)
    elif args.command == "ips":
        extract_random_ips_for_software_version(args.input,
                                                software_version=args.software_version,
                                                count=int(args.ip_count))
    else:
        raise ValueError("Unknown command '%s'." % args.command)
